import React from 'react';

import AddForm from './Components/AddForm';
import TaskListCont from './Components/TaskListCont';
import Filter from './Components/Filter';

export const App = () => (
    <React.Fragment>
        <AddForm />
        <Filter />
        <TaskListCont />
    </React.Fragment>
);
