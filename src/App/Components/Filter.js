import React from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import PropTypes from 'prop-types';

import FormControlLabel from '@material-ui/core/FormControlLabel';
import Checkbox from '@material-ui/core/Checkbox';

import { setVisibilityFilter } from '../state/actions/Action';

class Filter extends React.Component {
    static propTypes = {
        setVisibilityFilter: PropTypes.func.isRequired,
        filter: PropTypes.string.isRequired
    };

    componentDidMount() {
        this.props.setVisibilityFilter('All');
    }

    filterChange = event => {
        this.props.setVisibilityFilter(event.target.value);
    };

    render() {
        return (
            <div className="filter">
                <div>
                    <FormControlLabel
                        control={
                            <Checkbox
                                checked={this.props.filter === 'All'}
                                onChange={this.filterChange}
                                value="All"
                                color="primary"
                            />
                        }
                        label="All"
                    />
                    <FormControlLabel
                        control={
                            <Checkbox
                                checked={this.props.filter === 'Low'}
                                onChange={this.filterChange}
                                value="Low"
                                style={{ color: 'green' }}
                            />
                        }
                        label="Low"
                    />
                    <FormControlLabel
                        control={
                            <Checkbox
                                checked={this.props.filter === 'Medium'}
                                onChange={this.filterChange}
                                value="Medium"
                                style={{ color: 'orange' }}
                            />
                        }
                        label="Medium"
                    />
                </div>
                <div>
                    <FormControlLabel
                        control={
                            <Checkbox
                                checked={this.props.filter === 'High'}
                                onChange={this.filterChange}
                                value="High"
                                style={{ color: 'red' }}
                            />
                        }
                        label="High"
                    />
                    <FormControlLabel
                        control={
                            <Checkbox
                                checked={this.props.filter === 'Done'}
                                onChange={this.filterChange}
                                value="Done"
                                color="primary"
                            />
                        }
                        label="Done"
                    />
                    <FormControlLabel
                        control={
                            <Checkbox
                                checked={this.props.filter === 'UnDone'}
                                onChange={this.filterChange}
                                value="UnDone"
                                color="primary"
                            />
                        }
                        label="UnDone"
                    />
                </div>
            </div>
        );
    }
}

const mapStateToProps = state => ({
    filter: state.visibilityFilter
});

const mapDispatchToProps = dispatch =>
    bindActionCreators({ setVisibilityFilter }, dispatch);

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(Filter);
