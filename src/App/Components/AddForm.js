import React from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import PropTypes from 'prop-types';

import Button from '@material-ui/core/Button';
import TextField from '@material-ui/core/TextField';
import MenuItem from '@material-ui/core/MenuItem';
import FormHelperText from '@material-ui/core/FormHelperText';
import FormControl from '@material-ui/core/FormControl';
import Select from '@material-ui/core/Select';

import { addTask } from '../state/actions/Action';

import { BigInputMoment } from 'react-input-moment';
import moment from 'moment';

class AddForm extends React.Component {
    static propTypes = {
        addTask: PropTypes.func.isRequired
    };

    state = {
        title: '',
        desc: '',
        priority: 'Medium',
        inputMoment: moment(),
        changeCompDate: false,
        compDate: ''
    };

    handleInputChange = event => {
        const target = event.target;
        const name = target.name;
        const value = target.value;
        this.setState({
            [name]: value
        });
    };

    onDatePickerVisible = () => {
        this.setState({
            changeCompDate: !this.state.changeCompDate
        });
    };

    momentChange = mom => {
        this.setState({
            inputMoment: mom,
            compDate: mom.format('YYYY-MM-DD ' + 'HH:mm')
        });
    };

    handleSubmit = event => {
        event.preventDefault();
        this.props.addTask(
            this.state.title,
            this.state.desc,
            this.state.priority,
            this.state.compDate
        );
        this.setState({
            title: '',
            desc: '',
            priority: 'Medium',
            changeCompDate: false,
            compDate: ''
        });
    };

    render() {
        const {
            title,
            desc,
            priority,
            compDate,
            changeCompDate,
            inputMoment
        } = this.state;

        return (
            <form className="add-form" onSubmit={this.handleSubmit}>
                <TextField
                    id="outlined-dense"
                    label="Enter Task title"
                    margin="dense"
                    variant="outlined"
                    onChange={this.handleInputChange}
                    value={title}
                    name="title"
                    autoComplete="off"
                />
                <TextField
                    id="outlined-multiline-flexible"
                    label="Enter description"
                    multiline
                    value={desc}
                    onChange={this.handleInputChange}
                    margin="normal"
                    variant="outlined"
                    name="desc"
                />
                <div className="add-form-footer">
                    <FormControl style={{ width: '100px' }}>
                        <Select
                            value={priority}
                            onChange={this.handleInputChange}
                            displayEmpty
                            name="priority"
                        >
                            <MenuItem value="Medium">Medium</MenuItem>
                            <MenuItem value="Low">Low</MenuItem>
                            <MenuItem value="High">High</MenuItem>
                        </Select>
                        <FormHelperText>Set Priority</FormHelperText>
                    </FormControl>
                    {changeCompDate ? (
                        <div className="wrapper">
                            <TextField
                                id="standard-dense"
                                value={compDate}
                                disabled
                            />
                            <BigInputMoment
                                moment={inputMoment}
                                onChange={mom => this.momentChange(mom)}
                            />
                        </div>
                    ) : (
                        <div className="wrapper">
                            <i
                                className="material-icons"
                                onClick={this.onDatePickerVisible}
                            >
                                calendar_today
                            </i>
                            <p>Set Complete Date</p>
                        </div>
                    )}
                    <Button
                        variant="contained"
                        color="primary"
                        disabled={!title || !desc}
                        type="submit"
                    >
                        Add Task
                    </Button>
                </div>
            </form>
        );
    }
}

const mapDispatchToProps = dispatch =>
    bindActionCreators({ addTask }, dispatch);

export default connect(
    null,
    mapDispatchToProps
)(AddForm);
