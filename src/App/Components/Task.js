import React, { Component } from 'react';
import PropTypes from 'prop-types';
import EditForm from './EditForm';

import moment from 'moment';

import Paper from '@material-ui/core/Paper';
import TextField from '@material-ui/core/TextField';
import Icon from '@material-ui/core/Icon';
import Button from '@material-ui/core/Button';
import IconButton from '@material-ui/core/IconButton';
import Typography from '@material-ui/core/Typography';

export default class Task extends Component {
    static propTypes = {
        id: PropTypes.string.isRequired,
        title: PropTypes.string.isRequired,
        desc: PropTypes.string.isRequired,
        status: PropTypes.bool.isRequired,
        priority: PropTypes.string.isRequired,
        createDate: PropTypes.string.isRequired,
        doneDate: PropTypes.string.isRequired,
        completeDate: PropTypes.string.isRequired,
        delTask: PropTypes.func.isRequired,
        editTask: PropTypes.func.isRequired,
        statusTask: PropTypes.func.isRequired
    };

    state = {
        title: '',
        desc: '',
        delButtonPush: false,
        editButtonPush: false
    };

    componentDidMount() {
        this.setState({
            title: this.props.title,
            desc: this.props.desc
        });
    }

    onButtonEditPush = () => {
        this.setState({
            editButtonPush: !this.state.editButtonPush
        });
    };

    taskUpdate = (title, desc) => {
        this.setState({
            title: title,
            desc: desc
        });
    };

    render() {
        const {
            id,
            priority,
            createDate,
            completeDate,
            doneDate,
            delTask,
            status,
            statusTask,
            editTask
        } = this.props;

        let renderElement = null;

        if (this.state.editButtonPush) {
            renderElement = (
                <EditForm
                    id={id}
                    title={this.state.title}
                    desc={this.state.desc}
                    editTask={editTask}
                    onButtonEditPush={this.onButtonEditPush}
                    taskUpdate={this.taskUpdate}
                />
            );
        } else {
            renderElement = (
                <React.Fragment>
                    <TextField
                        className="title-field"
                        id="standard-dense"
                        value={this.state.title}
                        disabled
                    />
                    <TextField
                        className="textarea-field"
                        id="standard-multiline"
                        multiline
                        value={this.state.desc}
                        disabled
                    />
                </React.Fragment>
            );
        }

        return (
            <Paper
                elevation={3}
                className="task"
                style={
                    completeDate && moment(completeDate).isBefore(createDate)
                        ? { backgroundColor: '#ff9191' }
                        : null
                }
            >
                <div className="fields">
                    <IconButton>
                        <Icon
                            style={
                                status ? { color: 'green' } : { color: 'red' }
                            }
                            className="material-icons"
                            fontSize="large"
                            onClick={() => statusTask(id)}
                        >
                            done
                        </Icon>
                    </IconButton>
                    {renderElement}
                </div>
                <div className="task-footer">
                    <div className="info">
                        <Typography variant="title">
                            Priority: {priority}
                        </Typography>
                        <Typography variant="body1">
                            Create Date: {createDate}
                        </Typography>
                        {completeDate && (
                            <Typography variant="body1">
                                ToDO Date: {completeDate}
                            </Typography>
                        )}
                        {status && (
                            <Typography variant="body1">
                                Done date: {doneDate}
                            </Typography>
                        )}
                    </div>
                    <div className="buttons">
                        <Button
                            variant="fab"
                            color="primary"
                            onClick={this.onButtonEditPush}
                        >
                            <Icon>edit_icon</Icon>
                        </Button>
                        <Button
                            variant="fab"
                            color="secondary"
                            onClick={() => delTask(id)}
                        >
                            <Icon>delete_icon</Icon>
                        </Button>
                    </div>
                </div>
            </Paper>
        );
    }
}
