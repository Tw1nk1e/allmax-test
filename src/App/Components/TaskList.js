import React from 'react';
import PropTypes from 'prop-types';

import Task from './Task';

const TaskList = ({ filteredTasks, actions }) => (
    <div className="wrap">
        {filteredTasks.map(task => {
            return (
                <Task
                    key={task.id}
                    id={task.id}
                    title={task.title}
                    desc={task.desc}
                    priority={task.priority}
                    createDate={task.createDate}
                    doneDate={task.doneDate}
                    completeDate={task.completeDate}
                    status={task.status}
                    delTask={actions.delTask}
                    statusTask={actions.statusTask}
                    editTask={actions.editTask}
                />
            );
        })}
    </div>
);

TaskList.propTypes = {
    filteredTasks: PropTypes.array.isRequired,
    actions: PropTypes.object.isRequired
};

export default TaskList;
