import React, { Component } from 'react';
import PropTypes from 'prop-types';

import TextField from '@material-ui/core/TextField';

export default class EditForm extends Component {
    static propTypes = {
        title: PropTypes.string.isRequired,
        desc: PropTypes.string.isRequired,
        id: PropTypes.string.isRequired,
        editTask: PropTypes.func.isRequired,
        onButtonEditPush: PropTypes.func.isRequired,
        taskUpdate: PropTypes.func.isRequired
    };

    state = {
        title: this.props.title,
        desc: this.props.desc
    };

    handleInputChange = event => {
        const target = event.target;
        const name = target.name;
        const value = target.value;
        this.setState({
            [name]: value
        });
    };

    onSave = (id, title, desc) => () => {
        this.props.editTask(id, title, desc);
        this.props.onButtonEditPush();
        this.props.taskUpdate(title, desc);
    };

    render() {
        const { title, desc } = this.state;

        return (
            <div
                className="fields"
                onBlur={this.onSave(this.props.id, title, desc)}
            >
                <TextField
                    className="title-field"
                    onBlur={e => e.stopPropagation()}
                    id="standard-dense"
                    onChange={this.handleInputChange}
                    value={this.state.title}
                    name="title"
                    autoComplete="off"
                    autoFocus
                />
                <TextField
                    className="textarea-field"
                    id="standard-multiline"
                    multiline
                    value={this.state.desc}
                    onChange={this.handleInputChange}
                    name="desc"
                />
            </div>
        );
    }
}
