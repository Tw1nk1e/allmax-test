import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import * as Actions from '../state/actions/Action';
import { getVisibleTasks } from '../state/selector/filter';

import TaskList from './TaskList';

const mapStateToProps = state => ({
    filteredTasks: getVisibleTasks(state)
});

const mapDispatchToProps = dispatch => ({
    actions: bindActionCreators(Actions, dispatch)
});

const VisibleTaskList = connect(
    mapStateToProps,
    mapDispatchToProps
)(TaskList);

export default VisibleTaskList;
