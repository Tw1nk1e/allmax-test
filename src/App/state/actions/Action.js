export const ADD_TASK = 'ADD_NOTE';
export const DEL_TASK = 'DEL_NOTE';
export const EDIT_TASK = 'EDIT_TASK';
export const DONE_TASK = 'DONE_TASK';
export const SET_VISIBILITY_FILTER = 'SET_VISIBILITY_FILTER';

export const addTask = (title, desc, priority, compDate) => ({
    type: ADD_TASK,
    payload: { title, desc, priority, compDate }
});

export const delTask = id => ({
    type: DEL_TASK,
    payload: id
});

export const editTask = (id, title, desc) => ({
    type: EDIT_TASK,
    payload: { id, title, desc }
});

export const statusTask = id => ({
    type: DONE_TASK,
    payload: id
});

export const setVisibilityFilter = filter => ({
    type: SET_VISIBILITY_FILTER,
    filter
});
