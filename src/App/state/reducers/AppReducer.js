import { combineReducers } from 'redux';

import todoApp from './ToDoReducer';
import visibilityFilter from './FilterReducer';

export const AppReducer = combineReducers({
    todoApp,
    visibilityFilter
});