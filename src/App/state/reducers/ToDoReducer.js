import { ADD_TASK, DEL_TASK, EDIT_TASK, DONE_TASK } from '../actions/Action';

import uid from 'uid';
import moment from 'moment';

const initialState = [];

const todoApp = (state = initialState, action) => {
    let stateCopy = Array.from(state);

    switch (action.type) {
        case ADD_TASK:
            return [
                ...state,
                {
                    id: uid(),
                    status: false,
                    title: action.payload.title,
                    desc: action.payload.desc,
                    priority: action.payload.priority,
                    createDate: moment().format('YYYY-MM-DD ' + 'HH:mm'),
                    doneDate: '',
                    completeDate: action.payload.compDate
                }
            ];
        case DEL_TASK:
            return state.filter(item => item.id !== action.payload);
        case EDIT_TASK: {
            const item = stateCopy.findIndex(x => x.id === action.payload.id);
            stateCopy[item].title = action.payload.title;
            stateCopy[item].desc = action.payload.desc;

            return stateCopy;
        }
        case DONE_TASK: {
            const item = stateCopy.findIndex(x => x.id === action.payload);
            stateCopy[item].status = !stateCopy[item].status;
            stateCopy[item].doneDate = moment().format('YYYY-MM-DD ' + 'HH:mm');

            return stateCopy;
        }
        default:
            return state;
    }
};

export default todoApp;
