import { createSelector } from 'reselect';

const getFilter = state => state.visibilityFilter;
const getTasks = state => state.todoApp;

export const getVisibleTasks = createSelector(
    [getFilter, getTasks],
    (filter, items) => {
        switch (filter) {
            case 'All':
                return items;
            case 'High':
                return items.filter(task => task.priority === 'High');
            case 'Medium':
                return items.filter(task => task.priority === 'Medium');
            case 'Low':
                return items.filter(task => task.priority === 'Low');
            case 'Done':
                return items.filter(task => task.status === true);
            case 'UnDone':
                return items.filter(task => task.status === false);
        }
    }
);
